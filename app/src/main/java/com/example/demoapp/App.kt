package com.example.demoapp

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import com.example.demoapp.presentation.di.AppInjector
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel(this)
        AndroidThreeTen.init(this)
        initInjection(this)
    }

    private fun initInjection(application: Application) {
        startKoin {
            androidContext(application)
            modules(AppInjector.getInjectionModules())
            properties(AppInjector.getInjectionProperties())
        }
    }

    private fun createNotificationChannel(application: Application) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val id = application.getString(R.string.notification_channel_id)
            val name = application.getString(R.string.notification_channel_name)
            val descriptionText = application.getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(id, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager = application.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

}