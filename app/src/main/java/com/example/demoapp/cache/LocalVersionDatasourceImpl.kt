package com.example.demoapp.cache

import android.content.Context
import com.example.demoapp.data.datasource.LocalVersionDatasource
import com.example.demoapp.data.model.Version
import com.squareup.moshi.Moshi
import io.reactivex.Completable
import io.reactivex.Maybe
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Types.newParameterizedType
import okio.Buffer
import java.io.*


/**
 * Created by Kozma Zoltan Balazs on 2019-08-07.
 */

private const val ANDROID_VERSION_FILE_NAME = "android_versions.txt"

class LocalVersionDatasourceImpl(
        private val applicationContex: Context,
        private val moshi: Moshi
) : LocalVersionDatasource {

    @Throws(IOException::class)
    override fun getAndroidVersions(): Maybe<String> = Maybe.fromCallable {
        val file = File(applicationContex.getExternalFilesDir("/test_app"), ANDROID_VERSION_FILE_NAME)
        file.readText()
    }

    @Throws(IOException::class)
    override fun saveAndroidVersions(list: List<Version>): Completable = Completable.fromAction {
        val file = File(applicationContex.getExternalFilesDir("/test_app"), ANDROID_VERSION_FILE_NAME)

        val data = with(Buffer()) {
            val dataType = newParameterizedType(List::class.java, Version::class.java)
            val jsonAdapter: JsonAdapter<List<Version>> = moshi.adapter(dataType)
            val jsonWriter: JsonWriter = JsonWriter.of(this)
            jsonWriter.indent = "    "
            jsonAdapter.toJson(jsonWriter, list)
            return@with this.readUtf8()
        }

        FileOutputStream(file).use {
            it.write(data.toByteArray())
        }
    }

}