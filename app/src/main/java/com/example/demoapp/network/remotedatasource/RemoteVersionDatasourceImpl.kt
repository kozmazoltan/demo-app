package com.example.demoapp.network.remotedatasource

import com.example.demoapp.data.datasource.RemoteVersionDatasource
import com.example.demoapp.data.mapper.VersionMapper
import com.example.demoapp.data.model.Version
import com.example.demoapp.network.apiservice.AndroidVersionApiService
import io.reactivex.Single

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
class RemoteVersionDatasourceImpl(
    private val versionApiService: AndroidVersionApiService,
    private val versionMapper: VersionMapper
) : RemoteVersionDatasource {

    override fun getAndroidVersions(): Single<List<Version>> = versionApiService
        .getAndroidVersions()
        .map { versions -> versions.map { versionMapper.map(it) } }

}