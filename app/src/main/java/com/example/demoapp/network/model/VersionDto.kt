package com.example.demoapp.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
@JsonClass(generateAdapter = true)
data class VersionDto(
    @Json(name = "apiLevel") val apiLevel: Short,
    @Json(name = "codeName") val codeName: String,
    @Json(name = "imageUrl") val imageUrl: String,
    @Json(name = "releaseDate") val releaseDate: Long,
    @Json(name = "rowType") val rowType: Int,
    @Json(name = "versionNumber") val versionNumber: String
)