package com.example.demoapp.network.apiservice

import com.example.demoapp.network.model.VersionDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
interface AndroidVersionApiService {

    @GET("get/claNBwNqOG")
    fun getAndroidVersions(@Query("indent") indent: Int = 2): Single<List<VersionDto>>

}