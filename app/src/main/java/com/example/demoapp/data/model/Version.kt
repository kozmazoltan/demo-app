package com.example.demoapp.data.model


import org.threeten.bp.LocalDate

data class Version(
    val apiLevel: Short,
    val codeName: String,
    val imageUrl: String,
    val releaseDate: LocalDate,
    val versionNumber: String
)