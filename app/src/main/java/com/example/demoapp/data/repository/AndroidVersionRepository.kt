package com.example.demoapp.data.repository

import com.example.demoapp.data.datasource.LocalVersionDatasource
import com.example.demoapp.data.datasource.RemoteVersionDatasource
import com.example.demoapp.data.model.Version
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
class AndroidVersionRepository(
    private val remoteVersionDatasource: RemoteVersionDatasource,
    private val localVersionDatasource: LocalVersionDatasource
) {

    fun getAndroidVersionsList(): Single<List<Version>> = remoteVersionDatasource.getAndroidVersions()

    fun getAndroidVersionsJson(): Maybe<String> = remoteVersionDatasource.getAndroidVersions()
            .flatMapCompletable { localVersionDatasource.saveAndroidVersions(it) }
            .andThen(localVersionDatasource.getAndroidVersions())

}