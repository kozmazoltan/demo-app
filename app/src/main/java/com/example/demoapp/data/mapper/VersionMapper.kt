package com.example.demoapp.data.mapper

import com.example.demoapp.data.model.Version
import com.example.demoapp.network.model.VersionDto
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
class VersionMapper : Mapper<VersionDto, Version>{

    override fun map(item: VersionDto): Version = Version(
        item.apiLevel,
        item.codeName,
        item.imageUrl,
        Instant.ofEpochSecond(item.releaseDate).atZone(ZoneId.systemDefault()).toLocalDate(),
        item.versionNumber
    )

}