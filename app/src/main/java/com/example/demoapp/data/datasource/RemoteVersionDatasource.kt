package com.example.demoapp.data.datasource

import com.example.demoapp.data.model.Version
import io.reactivex.Single

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
interface RemoteVersionDatasource {

    fun getAndroidVersions(): Single<List<Version>>

}