package com.example.demoapp.data.datasource

import com.example.demoapp.data.model.Version
import io.reactivex.Completable
import io.reactivex.Maybe

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
interface LocalVersionDatasource {

    fun getAndroidVersions(): Maybe<String>

    fun saveAndroidVersions(list: List<Version>): Completable

}