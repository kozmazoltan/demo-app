package com.example.demoapp.data.mapper

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
interface Mapper<IN, OUT> {

    fun map(item: IN): OUT

    fun mapInverse(item: OUT): IN =
        throw NotImplementedError("Method should be implemented in child class before it is used")

}