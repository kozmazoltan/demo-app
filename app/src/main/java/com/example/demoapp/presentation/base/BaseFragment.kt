package com.example.demoapp.presentation.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
abstract class BaseFragment<VM : BaseViewModel> : Fragment() {

    protected abstract val viewModel: VM

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews(view)
        setupBaseEventObservables(viewLifecycleOwner)
        setupObservers(viewLifecycleOwner)
    }

    private fun setupBaseEventObservables(lifecycleOwner: LifecycleOwner) {
        viewModel.generalErrorObservable.observe(lifecycleOwner, Observer { showError(it) })
    }

    protected fun showError(exception: Throwable) {
        (activity as? BaseActivity)?.showErrorToast(exception)
    }

    abstract fun setupViews(view: View)

    abstract fun setupObservers(lifecycleOwner: LifecycleOwner)

}