package com.example.demoapp.presentation.base

import androidx.lifecycle.ViewModel
import com.example.demoapp.core.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
abstract class BaseViewModel : ViewModel() {

    val generalErrorObservable: SingleLiveEvent<Throwable> = SingleLiveEvent()
    private val disposables: CompositeDisposable = CompositeDisposable()

    protected fun showFailure(exception: Throwable) = generalErrorObservable.setValue(exception)

    protected fun addDisposable(disposable: Disposable) = disposables.add(disposable)

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

}