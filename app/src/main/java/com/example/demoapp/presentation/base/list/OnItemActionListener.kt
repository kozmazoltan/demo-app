package com.example.demoapp.presentation.base.list

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
interface OnItemActionListener<M> {

    fun onItemClicked(item: M)

}