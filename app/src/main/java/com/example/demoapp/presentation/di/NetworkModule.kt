package com.example.demoapp.presentation.di

import com.example.demoapp.network.apiservice.AndroidVersionApiService
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */

val networkModule = module {

    single { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY } }

//    single { NetworkMonitorImpl(get()) as NetworkMonitor }

//    single {
//        val networkMonitor: NetworkMonitor = get()
//        return@single NetworkInterceptor(networkMonitor)
//    }

    single { RxJava2CallAdapterFactory.create() }

    single<OkHttpClient> {
        val loggingInterceptor: HttpLoggingInterceptor = get()

        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build()
    }

    single { Moshi.Builder().build() }

    single { MoshiConverterFactory.create(get()) }

    single {
        val okHttpClient: OkHttpClient = get()
        val moshiConverterFactory: MoshiConverterFactory = get()
        val rxJavaCallAdapter: RxJava2CallAdapterFactory = get()
        val apiBaseUrl: String = getProperty(API_URL)

        return@single Retrofit.Builder()
            .baseUrl(apiBaseUrl)
            .client(okHttpClient)
            .addConverterFactory(moshiConverterFactory)
            .addCallAdapterFactory(rxJavaCallAdapter)
            .build()
    }

    single {
        val retrofit: Retrofit = get()
        return@single retrofit.create(AndroidVersionApiService::class.java)
    }

}