package com.example.demoapp.presentation.ui

import android.os.Bundle
import com.example.demoapp.R
import com.example.demoapp.presentation.base.BaseActivity
import com.example.demoapp.presentation.di.VERSIONS_PAGER_ADAPTER
import com.example.demoapp.presentation.service.BatteryService
import com.example.demoapp.presentation.utils.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named

class MainActivity : BaseActivity() {

    private val pagerAdapter: ViewPagerAdapter by inject(named(VERSIONS_PAGER_ADAPTER)) { parametersOf(supportFragmentManager) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpViews()
        BatteryService.startBatteryMonitoring(this)
    }

    private fun setUpViews() {
        vp_main_pager.adapter = pagerAdapter
        tl_main_tabs?.setupWithViewPager(vp_main_pager)
    }

    override fun onDestroy() {
        BatteryService.stopBatteryMonitoring(this)
        super.onDestroy()
    }

}
