package com.example.demoapp.presentation.di

import com.example.demoapp.data.mapper.VersionMapper
import com.example.demoapp.presentation.mapper.SelectableVersionMapper
import org.koin.dsl.module
import org.threeten.bp.format.DateTimeFormatter

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */

val mapperModule = module {

    factory { DateTimeFormatter.ofPattern("yyyy/MM/dd") }

    factory { VersionMapper() }

    factory { SelectableVersionMapper() }

}