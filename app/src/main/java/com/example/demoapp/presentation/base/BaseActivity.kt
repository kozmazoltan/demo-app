package com.example.demoapp.presentation.base

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
abstract class BaseActivity : AppCompatActivity() {

    fun showErrorToast(exception: Throwable) = Toast
        .makeText(this, getErrorMessage(exception), Toast.LENGTH_SHORT)
        .show()

    private fun getErrorMessage(exception: Throwable): String = exception.message ?: ""

}