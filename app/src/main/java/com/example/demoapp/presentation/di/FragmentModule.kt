package com.example.demoapp.presentation.di

import com.example.demoapp.presentation.ui.json.VersionJsonFragment
import com.example.demoapp.presentation.ui.list.VersionListFragment
import org.koin.dsl.module

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */

val fragmentModule = module {

    factory { VersionListFragment.newInstance() }

    factory { VersionJsonFragment.newInstance() }

}