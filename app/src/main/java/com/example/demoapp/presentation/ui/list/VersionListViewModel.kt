package com.example.demoapp.presentation.ui.list

import androidx.lifecycle.LiveData
import com.example.demoapp.core.SingleLiveEvent
import com.example.demoapp.data.model.Version
import com.example.demoapp.data.repository.AndroidVersionRepository
import com.example.demoapp.presentation.base.BaseViewModel
import com.example.demoapp.presentation.mapper.SelectableVersionMapper
import com.example.demoapp.presentation.model.SelectableVersion
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class VersionListViewModel(
    private val versionRepository: AndroidVersionRepository,
    private val selectableVersionMapper: SelectableVersionMapper
) : BaseViewModel() {

    private val _viewStateObservable: SingleLiveEvent<VersionListState> = SingleLiveEvent()
    val viewStateObservable: LiveData<VersionListState>
        get() = _viewStateObservable

    init {
        _viewStateObservable.value = VersionListState.Loading
        loadVersions()
    }

    fun loadVersions() = versionRepository.getAndroidVersionsList()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(::handleSuccess, ::handleError)
        .also { addDisposable(it) }

    fun onVersionSelected(version: SelectableVersion) {
        val previousItems = obtainPreviousData()
        val newItems = previousItems.map {
            if (it.version.versionNumber == version.version.versionNumber) SelectableVersion(it.version, !it.isSelected)
            else it
        }
        _viewStateObservable.value = VersionListState.Success(newItems)
    }

    private fun obtainPreviousData(): List<SelectableVersion> {
        val viewState = _viewStateObservable.value
        return if (viewState is VersionListState.Success) {
            viewState.items
        } else emptyList()
    }

    private fun handleSuccess(list: List<Version>) {
        val selectableList = list.map { selectableVersionMapper.map(it) }
        _viewStateObservable.value = VersionListState.Success(selectableList)
    }

    private fun handleError(exception: Throwable) {
        _viewStateObservable.value = VersionListState.Error
        showFailure(exception)
    }

}
