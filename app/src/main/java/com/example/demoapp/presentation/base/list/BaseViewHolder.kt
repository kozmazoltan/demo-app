package com.example.demoapp.presentation.base.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
abstract class BaseViewHolder<M, L : OnItemActionListener<M>>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(item: M, listener: L?, payload: List<Any>? = null)

}