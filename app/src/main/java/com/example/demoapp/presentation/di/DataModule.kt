package com.example.demoapp.presentation.di

import com.example.demoapp.cache.LocalVersionDatasourceImpl
import com.example.demoapp.data.datasource.LocalVersionDatasource
import com.example.demoapp.data.datasource.RemoteVersionDatasource
import com.example.demoapp.network.remotedatasource.RemoteVersionDatasourceImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */

val dataModule = module {

    single<RemoteVersionDatasource> {
        RemoteVersionDatasourceImpl(
            versionApiService = get(),
            versionMapper = get()
        )
    }

    single<LocalVersionDatasource> {
        LocalVersionDatasourceImpl(
                applicationContex = androidContext(),
                moshi = get()
        )
    }

}