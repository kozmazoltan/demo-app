package com.example.demoapp.presentation.di

import com.example.demoapp.data.repository.AndroidVersionRepository
import org.koin.dsl.module

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */

val repositoryModule = module {

    single {
        AndroidVersionRepository(
                remoteVersionDatasource = get(),
                localVersionDatasource = get()
        )
    }

}