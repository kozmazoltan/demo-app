package com.example.demoapp.presentation.di

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demoapp.R
import com.example.demoapp.presentation.ui.json.VersionJsonFragment
import com.example.demoapp.presentation.ui.list.VersionAdapter
import com.example.demoapp.presentation.ui.list.VersionListFragment
import com.example.demoapp.presentation.utils.ViewPagerAdapter
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */

const val VERSIONS_PAGER_ADAPTER = "versions_pager_adapter"

val viewComponentModule = module {

    factory(named(VERSIONS_PAGER_ADAPTER)) {(fragmentManager: FragmentManager) ->
        val fragments = listOf(get<VersionListFragment>(), get<VersionJsonFragment>())
        val titles = listOf(
                androidContext().getString(R.string.version_list_title),
                androidContext().getString(R.string.version_json_title)
        )

        ViewPagerAdapter(fragmentManager, fragments, titles)
    }

    factory { (context: Context) -> LinearLayoutManager(context, RecyclerView.VERTICAL, false) }

    factory { VersionAdapter(dateTimeFormatter = get()) }

}