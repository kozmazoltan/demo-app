package com.example.demoapp.presentation.ui.json

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.example.demoapp.R
import com.example.demoapp.core.extensions.gone
import com.example.demoapp.core.extensions.visible
import com.example.demoapp.presentation.base.BaseFragment
import org.koin.android.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.fragment_version_json.view.*

class VersionJsonFragment : BaseFragment<VersionJsonViewModel>() {

    override val viewModel: VersionJsonViewModel by viewModel()

    companion object {
        fun newInstance() = VersionJsonFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_version_json, container, false)
    }

    override fun setupViews(view: View) {

    }

    override fun setupObservers(lifecycleOwner: LifecycleOwner) {
        viewModel.viewStateObservable.observe(lifecycleOwner, Observer { handleViewState(it) })
    }

    private fun handleViewState(viewState: VersionJsonState) {
        when(viewState) {
            is VersionJsonState.Loading -> showLoading()
            is VersionJsonState.NeedPermission -> getStoragePermission()
            is VersionJsonState.Success -> {
                hideLoading()
                showData(viewState.json)
            }
            is VersionJsonState.Error -> hideLoading()
        }
    }

    private fun showLoading() = view?.pb_versions?.visible()

    private fun hideLoading() = view?.pb_versions?.gone()

    private fun showData(json: String) = view?.tv_versions?.setText(json)

    private fun getStoragePermission() {

        fun askForPermission() = requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                WRITE_PERMISSION_REQUEST_CODE
        )

        if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE))
            AlertDialog.Builder(requireContext())
                    .setTitle(R.string.popup_title)
                    .setMessage(R.string.popup_permission)
                    .setPositiveButton(R.string.popup_positive) { dialog, _ ->
                        dialog.dismiss()
                        askForPermission()
                    }
                    .show()
        else
            askForPermission()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        viewModel.onPermissionResult(requestCode, permissions, grantResults)
    }

}
