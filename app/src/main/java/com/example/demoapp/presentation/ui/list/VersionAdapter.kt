package com.example.demoapp.presentation.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.example.demoapp.R
import com.example.demoapp.presentation.base.list.BaseViewHolder
import com.example.demoapp.presentation.base.list.OnItemActionListener
import com.example.demoapp.presentation.model.SelectableVersion
import kotlinx.android.synthetic.main.item_list_versions.view.*
import org.threeten.bp.format.DateTimeFormatter

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
class VersionAdapter(
    private val dateTimeFormatter: DateTimeFormatter
) : ListAdapter<SelectableVersion, VersionAdapter.VersionViewHolder>(object: DiffUtil.ItemCallback<SelectableVersion>() {

    override fun areItemsTheSame(oldItem: SelectableVersion, newItem: SelectableVersion): Boolean =
        oldItem.version.versionNumber == newItem.version.versionNumber

    override fun areContentsTheSame(oldItem: SelectableVersion, newItem: SelectableVersion): Boolean =
        oldItem.isSelected == newItem.isSelected

}) {

    private var onItemActionListener: OnItemActionListener<SelectableVersion>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VersionViewHolder = LayoutInflater
        .from(parent.context)
        .inflate(R.layout.item_list_versions, parent, false)
        .let { view -> VersionViewHolder(view, dateTimeFormatter) }

    override fun onBindViewHolder(holder: VersionViewHolder, position: Int) = holder
        .bind(getItem(position), onItemActionListener, null)
    
    fun setOnItemActionListener(onItemActionListener: OnItemActionListener<SelectableVersion>) {
        this.onItemActionListener = onItemActionListener
    }
    
    class VersionViewHolder(
        itemView: View,
        private val dateTimeFormatter: DateTimeFormatter
    ) : BaseViewHolder<SelectableVersion, OnItemActionListener<SelectableVersion>>(itemView) {

        override fun bind(item: SelectableVersion, listener: OnItemActionListener<SelectableVersion>?, payload: List<Any>?) {
            itemView.isSelected = item.isSelected
            itemView.cl_version_item.setOnClickListener { listener?.onItemClicked(item) }
            itemView.tv_version_name.text = item.version.codeName
            itemView.tv_version_api_level.text = item.version.apiLevel.toString()
            itemView.tv_version_code.text = item.version.versionNumber
            itemView.tv_version_release_date.text = dateTimeFormatter.format(item.version.releaseDate)
            with(itemView.iv_version_picture) {
                Glide.with(this).load(item.version.imageUrl).into(this)
            }
        }

    }

}