package com.example.demoapp.presentation.di

import com.example.demoapp.BuildConfig
import org.koin.core.module.Module

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */

const val API_URL = "api_url"

class AppInjector {

    companion object {

        fun getInjectionProperties(): HashMap<String, Any> = hashMapOf(
            API_URL to BuildConfig.API_URL
        )

        fun getInjectionModules(): List<Module> {

            fun getViewModelModules(): List<Module> = listOf(
                viewModelModule
            )

            fun getDataModules(): List<Module> = listOf(
                repositoryModule,
                dataModule,
                mapperModule,
                networkModule
            )

            fun getViewComponentModules(): List<Module> = listOf(
                viewComponentModule,
                fragmentModule
            )

            fun getFrameworkComponentModules(): List<Module> = listOf(
//                    frameworkModule
            )

            val modules = mutableListOf<Module>()
            modules.addAll(getViewModelModules())
            modules.addAll(getDataModules())
            modules.addAll(getViewComponentModules())
            modules.addAll(getFrameworkComponentModules())
            return modules

        }

    }

}