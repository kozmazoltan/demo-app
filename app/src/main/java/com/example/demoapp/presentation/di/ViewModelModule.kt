package com.example.demoapp.presentation.di

import com.example.demoapp.presentation.ui.json.VersionJsonViewModel
import com.example.demoapp.presentation.ui.list.VersionListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */

val viewModelModule = module {

    viewModel {
        VersionListViewModel(
                versionRepository = get(),
                selectableVersionMapper = get()
        )
    }

    viewModel {
        VersionJsonViewModel(
                applicationContext = androidContext(),
                versionRepository = get()
        )
    }

}