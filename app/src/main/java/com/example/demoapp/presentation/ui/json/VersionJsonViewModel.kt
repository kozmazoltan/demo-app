package com.example.demoapp.presentation.ui.json

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import com.example.demoapp.core.SingleLiveEvent
import com.example.demoapp.core.extensions.logDebug
import com.example.demoapp.data.repository.AndroidVersionRepository
import com.example.demoapp.presentation.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

const val WRITE_PERMISSION_REQUEST_CODE = 101

class VersionJsonViewModel(
        private val applicationContext: Context,
        private val versionRepository: AndroidVersionRepository
) : BaseViewModel() {

    private val _viewStateObservable: SingleLiveEvent<VersionJsonState> = SingleLiveEvent()
    val viewStateObservable: LiveData<VersionJsonState>
        get() = _viewStateObservable

    init {
        if (hasStoragePermission()){
            startLoading()
        } else {
            _viewStateObservable.value = VersionJsonState.NeedPermission
        }

    }

    private fun startLoading() {
        _viewStateObservable.value = VersionJsonState.Loading
        loadVersions()
    }

    private fun loadVersions() = versionRepository.getAndroidVersionsJson()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(::handleSuccess, ::handleError)
            .also { addDisposable(it) }

    private fun handleSuccess(json: String) {
        _viewStateObservable.value = VersionJsonState.Success(json)
    }

    private fun handleError(exception: Throwable) {
        _viewStateObservable.value = VersionJsonState.Error
        showFailure(exception)
    }

    private fun hasStoragePermission(): Boolean =
            ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

    fun onPermissionResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            WRITE_PERMISSION_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startLoading()
                } else {
                    _viewStateObservable.value = VersionJsonState.NeedPermission
                }
                return
            }
        }
    }

}
