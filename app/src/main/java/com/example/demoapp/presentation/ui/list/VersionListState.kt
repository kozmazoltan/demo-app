package com.example.demoapp.presentation.ui.list

import com.example.demoapp.presentation.model.SelectableVersion

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
sealed class VersionListState {

    object Loading : VersionListState()
    data class Success(val items: List<SelectableVersion>): VersionListState()
    object Error : VersionListState()

}