package com.example.demoapp.presentation.model

import com.example.demoapp.data.model.Version

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
data class SelectableVersion(
    val version: Version,
    val isSelected: Boolean
)