package com.example.demoapp.presentation.mapper

import com.example.demoapp.data.mapper.Mapper
import com.example.demoapp.data.model.Version
import com.example.demoapp.presentation.model.SelectableVersion

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
class SelectableVersionMapper : Mapper<Version, SelectableVersion> {

    override fun map(item: Version): SelectableVersion = SelectableVersion(item, false)

}