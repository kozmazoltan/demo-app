package com.example.demoapp.presentation.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
class ViewPagerAdapter(
        fragmentManager: FragmentManager,
        private val fragments: List<Fragment>,
        private val titles: List<String>?
) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence? = titles?.get(position)
}