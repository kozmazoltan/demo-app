package com.example.demoapp.presentation.service

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.os.BatteryManager
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.example.demoapp.R
import com.example.demoapp.core.extensions.logDebug
import com.example.demoapp.presentation.ui.MainActivity
import kotlin.math.roundToInt

/**
 * Created by Kozma Zoltan Balazs on 2019-08-06.
 */
private const val ACTION_START_BATTERY_SERVICE: String = "action_start_battery_service"
private const val ACTION_STOP_BATTERY_SERVICE: String = "action_stop_battery_service"
private const val NOTIFICATION_ID: Int = 1001

class BatteryService : Service() {

    companion object {

        fun startBatteryMonitoring(context: Context) {
            val intent = Intent(context, BatteryService::class.java)
            intent.action = ACTION_START_BATTERY_SERVICE
            context.startService(intent)
        }

        fun stopBatteryMonitoring(context: Context) {
            val intent = Intent(context, BatteryService::class.java)
            intent.action = ACTION_STOP_BATTERY_SERVICE
            context.startService(intent)
        }

    }

    private val notificationBuilder: NotificationCompat.Builder by lazy {
        val channelId = applicationContext.getString(R.string.notification_channel_id)

        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        return@lazy NotificationCompat.Builder(applicationContext, channelId)
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
    }

    private val batteryStateChangeReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let { handleBatteryIntent(it) }
            logDebug("Battery receiver")
        }

    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            when (it.action) {
                ACTION_START_BATTERY_SERVICE -> startForegroundService()
                ACTION_STOP_BATTERY_SERVICE -> stopForegroundService()
            }
        }
        return START_STICKY_COMPATIBILITY
    }

    fun handleBatteryIntent(batteryIntent: Intent) {

        fun getBatteryPercentage(batteryIntent: Intent): String {
            val level: Int = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
            val scale: Int = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, 0)

            val percent: Int = ((level / scale.toFloat()) * 100).roundToInt()
            return applicationContext.getString(R.string.battery_level, percent)
        }

        fun getChargingState(batteryIntent: Intent): String {
            val status: Int = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
            val isCharging: Boolean = status == BatteryManager.BATTERY_STATUS_CHARGING
                    || status == BatteryManager.BATTERY_STATUS_FULL
            return when (isCharging) {
                true -> applicationContext.getString(R.string.battery_state_charging)
                false -> applicationContext.getString(R.string.battery_state_not_charging)
            }
        }

        fun updateNotification() {
            (applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
                    .notify(NOTIFICATION_ID, notificationBuilder.build())
        }

        batteryIntent.let {
            notificationBuilder.setContentTitle(getBatteryPercentage(it))
            notificationBuilder.setContentText(getChargingState(it))
        }.also {
            updateNotification()
        }
    }

    private fun registerBatteryUpdate() = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
            .let { intentFilter ->
                applicationContext.registerReceiver(batteryStateChangeReceiver, intentFilter)
            }?.let {
                logDebug("Battery intent")
                handleBatteryIntent(it)
            }


    private fun unregisterBatteryUpdate() = applicationContext.unregisterReceiver(batteryStateChangeReceiver)

    private fun startForegroundService() {
        logDebug("Start battery service.")
        registerBatteryUpdate()
        startForeground(NOTIFICATION_ID, notificationBuilder.build())
    }

    private fun stopForegroundService() {
        logDebug("Stop battery service.")
        unregisterBatteryUpdate()
        stopForeground(true)
        stopSelf()
    }

}