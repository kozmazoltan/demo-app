package com.example.demoapp.presentation.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demoapp.R
import com.example.demoapp.presentation.base.BaseFragment
import com.example.demoapp.presentation.base.list.OnItemActionListener
import com.example.demoapp.presentation.model.SelectableVersion
import kotlinx.android.synthetic.main.fragment_version_list.view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class VersionListFragment : BaseFragment<VersionListViewModel>() {

    override val viewModel: VersionListViewModel by viewModel()

    private val versionAdapter: VersionAdapter by inject()


    companion object {
        fun newInstance() = VersionListFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_version_list, container, false)
    }

    override fun setupViews(view: View) {
        versionAdapter.setOnItemActionListener(object : OnItemActionListener<SelectableVersion> {
            override fun onItemClicked(item: SelectableVersion) {
                viewModel.onVersionSelected(item)
            }

        })
        view.rv_versions?.apply {
            this.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            this.adapter = versionAdapter
            this.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        }
        view.srl_versions?.setOnRefreshListener { viewModel.loadVersions() }
    }

    override fun setupObservers(lifecycleOwner: LifecycleOwner) {
        viewModel.viewStateObservable.observe(lifecycleOwner, Observer { handleViewState(it) })
    }

    private fun handleViewState(viewState: VersionListState) {
        when(viewState) {
            is VersionListState.Loading -> showLoading()
            is VersionListState.Success -> {
                hideLoading()
                showData(viewState.items)
            }
            is VersionListState.Error -> hideLoading()
        }
    }

    private fun showLoading() = view?.srl_versions?.setRefreshing(true)

    private fun hideLoading() = view?.srl_versions?.setRefreshing(false)

    private fun showData(items: List<SelectableVersion>) = versionAdapter.submitList(items)

}
