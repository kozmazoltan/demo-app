package com.example.demoapp.presentation.ui.json

/**
 * Created by Kozma Zoltan Balazs on 2019-08-07.
 */
sealed class VersionJsonState {

    object Loading : VersionJsonState()
    object NeedPermission: VersionJsonState()
    data class Success(val json: String): VersionJsonState()
    object Error: VersionJsonState()

}