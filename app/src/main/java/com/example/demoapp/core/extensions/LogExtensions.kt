package com.example.demoapp.core.extensions

import android.util.Log
import com.example.demoapp.BuildConfig.DEBUG

/**
 * Created by Kozma Zoltan Balazs on 2019-08-07.
 */

private const val APP_NAME = "TEST_APP"

fun Any.logDebug(message: String, tag: String? = null) {
    if (DEBUG) {
        val logTag: String = tag ?: javaClass.simpleName
        logD(APP_NAME, "[$logTag]: $message")
    }
}

private fun logD(tag: String, logMsg: String) {
    if (logMsg.length > 4000) {
        val chunks = logMsg.chunked(4000)
        chunks.forEach { Log.d(tag, it) }
    } else
        Log.d(tag, logMsg)
}