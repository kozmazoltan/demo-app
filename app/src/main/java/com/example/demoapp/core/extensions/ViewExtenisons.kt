package com.example.demoapp.core.extensions

import android.view.View

/**
 * Created by Kozma Zoltan Balazs on 2019-08-07.
 */

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}